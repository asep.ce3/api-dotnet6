using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplicationCrud.Data;

namespace WebApplicationCrud.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SuperheroController : ControllerBase
{
    private DataContext _context;
    public SuperheroController(DataContext context)
    {
        _context = context;
    }

    [HttpGet]
    public async Task<ActionResult<List<Superhero>>> Get()
    {
        return Ok(await _context.Superheros.ToListAsync());
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Superhero>> Get(int id)
    {
        var hero = await _context.Superheros.FindAsync(id);
        if (hero == null)
            return BadRequest("hero not found");

        return Ok(hero);
    }

    [HttpPost]
    public async Task<ActionResult<List<Superhero>>> AddHero(Superhero hero)
    {
        await _context.Superheros.AddAsync(hero);
        await _context.SaveChangesAsync();
        return Ok(await _context.Superheros.ToListAsync());
    }

    [HttpPut]
    public async Task<ActionResult<List<Superhero>>> UpdateHero(Superhero request)
    {
        var hero = await _context.Superheros.FindAsync(request.Id);
        if (hero == null)
            return BadRequest("Hero not found");

        hero.Name = request.Name;
        hero.FirstName = request.FirstName;
        hero.LastName = request.LastName;
        hero.Place = request.Place;
        await _context.SaveChangesAsync();

        return Ok(await _context.Superheros.ToListAsync());
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<List<Superhero>>> DeleteHero(int id)
    {
        var hero = await _context.Superheros.FindAsync(id);
        if (hero == null)
            return BadRequest("hero not found.");

        _context.Superheros.Remove(hero);
        await _context.SaveChangesAsync();
        return Ok(await _context.Superheros.ToListAsync());
    }
}