using Microsoft.EntityFrameworkCore;

namespace WebApplicationCrud.Data;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options) : base(options) { }
    
    public DbSet<Superhero> Superheros { get; set; }
}